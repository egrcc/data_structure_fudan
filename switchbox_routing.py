class Switchbox:
    def __init__(self, net):
        self.net = net

    def check_box(self):
        stack_list = []
        i = 0
        while i < len(net):
            if(len(stack_list) == 0):
                stack_list.append(net[i])
            else:
                stack_list.append(net[i])
                if(stack_list[len(stack_list) - 1] == stack_list[len(stack_list) - 2]):
                    stack_list.pop()
                    stack_list.pop()

            if(i == len(net) - 1):
                if(len(stack_list) == 0):
                    print "Switch box is routable"
                else:
                    print "Switch box is not routable"
            i = i + 1

if __name__ == "__main__":
    net = [1, 2, 2, 1, 3, 3, 4, 4]
    switch_box = Switchbox(net)
    switch_box.check_box()
