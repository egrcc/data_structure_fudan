import random

class Heap(list):

    def __init__(self, array = None):
        if array == None:
            super(Heap, self).__init__()
        else:
            super(Heap, self).__init__(array)
        self.size = 0

    def set_size(self, size):
        self.size = size
        

def parent(i):
    return int(i / 2)

def left(i):
    return 2 * i

def right(i):
    return 2 * i + 1

def max_heapify(heap, i):
    l = left(i)
    r = right(i)
    if l <= heap.size and heap[l - 1] > heap[i - 1]:
        largest = l
    else:
        largest = i
    if r <= heap.size and heap[r - 1] > heap[largest - 1]:
        largest = r
    if largest != i:
        temp = heap[i - 1]
        heap[i - 1] = heap[largest - 1]
        heap[largest - 1] = temp
        max_heapify(heap, largest)

def bulid_max_heap(heap):
    heap.set_size(len(heap))
    for i in range(1, (len(heap) / 2) + 1)[::-1]:
        max_heapify(heap, i)

def sort(heap):
    bulid_max_heap(heap)
    for i in range(2, len(heap) + 1)[::-1]:
        temp = heap[0]
        heap[0] = heap[i - 1]
        heap[i - 1] = temp
        heap.set_size(heap.size - 1)
        max_heapify(heap, 1)



heap = Heap()
for i in range(20):
    heap.append(random.randint(1, 1000))

print heap
sort(heap)
print heap
