import random

NUMBER = 20
array = []
for i in range(NUMBER):
    array.append(random.randint(1,1000))

def merge(array, p, q, r):

    n = q - p + 1
    m = r - q
    L = []
    R = []

    for i in range(n):
        L.append(array[p + i])
    for j in range(m):
        R.append(array[q + 1 + j])
    L.append(float("inf"))
    R.append(float("inf"))

    i = 0
    j = 0
    for k in range(p, r + 1):
        if L[i] < R[j]:
            array[k] = L[i]
            i = i + 1
        else:
            array[k] = R[j]
            j = j + 1

def sort(array, p, r):
    if p < r:
        q = int((p + r) / 2)
        sort(array, p, q)
        sort(array, q + 1, r)
        merge(array, p, q, r)


if __name__ == "__main__":
    print array
    # merge(array, 3, 4, 6)
    p = 0
    r = NUMBER - 1
    sort(array, p, r)
    print array