import random

def partition(array, p, r):
    x = array[r]
    i = p - 1
    for j in range(p, r):
        if array[j] <= x:
            i = i + 1
            temp = array[j]
            array[j] = array[i]
            array[i] = temp
    temp = array[i + 1]
    array[i + 1] = x
    array[r] = temp
    return i + 1

def sort(array, p, r):
    if p < r:
        q = partition(array, p, r)
        sort(array, p, q - 1)
        sort(array, q + 1, r)

def randomized_partition(array, p, r):
    i = random.randint(p, r)
    temp = array[i]
    array[i] = array[r]
    array[r] = temp
    return partition(array, p, r)

def randomized_sort(array, p, r):
    if p < r:
        q = randomized_partition(array, p, r)
        randomized_sort(array, p, q - 1)
        randomized_sort(array, q + 1, r)

array = []
for i in range(20):
    array.append(random.randint(1, 1000))
print array
sort(array, 0, len(array) - 1)
print array